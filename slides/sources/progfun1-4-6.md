% Variance
%
%

Variance
========

You have seen the the previous session that some types should be covariant whereas
others should not.

Roughly speaking, a type that accepts mutations of its elements should
not be covariant.

But immutable types can be covariant, if some conditions
on methods are met.

Definition of Variance
======================

Say `C[T]` is a parameterized type and `A`, `B` are types such that `A <: B`.

In general, there are _three_ possible relationships between `C[A]` and `C[B]`:

\begin{tabular}{p{8cm}l}
    \verb@C[A] <: C[B]@  & \verb@C@ is \red{covariant}
\\  \verb@C[A] >: C[B]@  & \verb@C@ is \red{contravariant}
\\  neither \verb@C[A]@ nor \verb@C[B]@ is a subtype of the other & \verb@C@ is \red{nonvariant}
\end{tabular}\medskip

->

Scala lets you declare the variance of a type by annotating the type parameter:

\begin{tabular}{p{8cm}l}
    \verb@class C[+A] { ... }@  & \verb@C@ is \red{covariant}
\\  \verb@class C[-A] { ... }@  & \verb@C@ is \red{contravariant}
\\  \verb@class C[A] { ... }         @ & \verb@C@ is \red{nonvariant}
\end{tabular}

Exercise
========

Assume the following type hierarchy and two function types:

       trait Fruit
       class Apple extends Fruit
       class Orange extends Fruit

       type A = Fruit => Orange
       type B = Apple => Fruit

According to the Liskov Substitution Principle, which of the
following should be true?

     O         A <: B
     O         B <: A
     O         A and B are unrelated.
->
\quiz

Exercise
========

Assume the following type hierarchy and two function types:

       trait Fruit
       class Apple extends Fruit
       class Orange extends Fruit

       type A = Fruit => Orange
       type B = Apple => Fruit

According to the Liskov Substitution Principle, which of the
following should be true?

     O         A <: B
     X         B <: A
     O         A and B are unrelated.

Typing Rules for Functions
==========================

Generally, we have the following rule for subtyping between function types:

If `A2 <: A1` and `B1 <: B2`, then

      A1 => B1  <:  A2 => B2

Function Trait Declaration
==========================

So functions are _contravariant_ in their argument type(s) and
_covariant_ in their result type.

This leads to the following revised definition of the `Function1` trait:

      package scala
      trait Function1[-T, +U] {
        def apply(x: T): U
      }

Variance Checks
===============

We have seen in the array example that the combination of covariance with
certain operations is unsound.

In this case the problematic operation was the update operation on an array.

If we turn `Array` into a class, and `update` into a method, it would look like this:

      class Array[+T] {
        def update(x: T) = ...
      }

The problematic combination is

 - the covariant type parameter `T`
 - which appears in parameter position of the method `update`.

Variance Checks (2)
===================

The Scala compiler will check that there are no problematic combinations when compiling a class with variance annotations.

Roughly,

 - _covariant_ type parameters can only appear in method results.
 - _contravariant_ type parameters can only appear in method parameters.
 - _invariant_ type parameters can appear anywhere.

The precise rules are a bit more involved, fortunately the Scala compiler performs them for us.

Variance-Checking the Function Trait
====================================

Let's have a look again at Function1:

      trait Function1[-T, +U]
        def apply(x: T): U

Here,

 - `T` is contravariant and appears only as a method parameter type
 - `U` is covariant and appears only as a method result type

So the method is checks out OK.

Variance and Lists
==================

Let's get back to the previous implementation of lists.

One shortcoming was that `Nil` had to be a class, whereas we would
prefer it to be an object (after all, there is only one empty list).

Can we change that?

Yes, because we can make `List` covariant.
->
Here are the essential modifications:

      trait List[+T]
        ...
      object Empty extends List[Nothing]
        ...

Making Classes Covariant
========================

Sometimes, we have to put in a bit of work to make a class covariant.

Consider adding a `prepend` method to `List` which prepends a given
element, yielding a new list.

A first implementation of `prepend` could look like this:

      trait List[+T]
        def prepend(elem: T): List[T] = Node(elem, this)

But that does not work!

Exercise
========

Why does the following code not type-check?

      trait List[+T]
        def prepend(elem: T): List[T] = Node(elem, this)

Possible answers:

  `O` \tab    `prepend` turns `List` into a mutable class.

  `O` \tab    `prepend` fails variance checking.

  `O` \tab    `prepend`'s right-hand side contains a type error.
->

\quiz

Prepend Violates LSP
====================

Indeed, the compiler is right to throw out `List` with `prepend`,
because it violates the Liskov Substitution Principle:

Here's something one can do with a list `xs` of type `List[Fruit]`:

      xs.prepend(Orange)

But the same operation on a list `ys` of type `List[Apple]` would lead to a type error:

      ys.prepend(Apple)
                 ^ type mismatch
                 required: Apple
                 found   : Orange

So, `List[Apple]` cannot be a subtype of `List[Fruit]`.

Lower Bounds
============

But `prepend` is a natural method to have on immutable lists!

\red{Q}: How can we make it variance-correct?
\medskip

->
We can use a _lower bound_:

      def prepend [U >: T] (elem: U): List[U] = Node(elem, this)

This passes variance checks, because:

 - _covariant_ type parameters may appear in _lower bounds_ of method type parameters
 - _contravariant_ type parameters may appear in _upper bounds_.

Exercise
========

Assume `prepend` in trait `List` is implemented like this:

      def prepend [U >: T] (elem: U): List[U] = Node(elem, this)

What is the result type of this function:

      def f(xs: List[Apple], x: Orange) = xs.prepend(x)   ?
\medskip

Possible answers:

\begin{tabular}{ll}
   \verb`  O         ` &       does not type check
\\ \verb`  O         ` &       \verb`List[Apple]`
\\ \verb`  O         ` &       \verb`List[Orange]`
\\ \verb`  O         ` &       \verb`List[Fruit]`
\\ \verb`  O         ` &       \verb`List[Any]`
\end{tabular}
->
\quiz

Exercise
========

Assume `prepend` in trait `List` is implemented like this:

      def prepend [U >: T] (elem: U): List[U] = Node(elem, this)

What is the result type of this function:

      def f(xs: List[Apple], x: Orange) = xs.prepend(x)   ?
\medskip

Possible answers:

\begin{tabular}{ll}
   \verb`  O         ` &       does not type check
\\ \verb`  O         ` &       \verb`List[Apple]`
\\ \verb`  O         ` &       \verb`List[Orange]`
\\ \verb`  X         ` &       \verb`List[Fruit]`
\\ \verb`  O         ` &       \verb`List[Any]`
\end{tabular}
