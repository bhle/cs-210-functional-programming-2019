% Lists
%
%
Lists
=====

The list is a fundamental data structure in functional programming.

A list having $\btt x_1, ..., x_n$
as elements is written `List(`$\btt x_1, ..., x_n$`)`

\example

      val fruit  = List("apples", "oranges", "pears")
      val nums   = List(1, 2, 3, 4)
      val diag3  = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
      val empty  = List()

There are two important differences between lists and arrays.

 - Lists are immutable --- the elements of a list cannot be changed.
 - Lists are recursive, while arrays are flat.

Lists
=====

      val fruit  = List("apples", "oranges", "pears")
      val diag3  = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))

\vspace{2cm}
$$
\mbox{(Drawing on Blackboard)}
$$

The List Type
=============

Like arrays, lists are \red{\em homogeneous}: the elements of a
list must all have the same type.

The type of a list with elements of type `T` is written `scala.List[T]` or shorter just `List[T]`

\example

      val fruit: List[String]    = List("apples", "oranges", "pears")
      val nums : List[Int]       = List(1, 2, 3, 4)
      val diag3: List[List[Int]] = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1))
      val empty: List[Nothing]   = List()

Constructors of Lists
=====================

All lists are constructed from:

 - the empty list `Nil`, and
 - the construction operation `::` (pronounced \emph{cons}): \newline
`x :: xs` gives a new list with the first element `x`, followed by the elements of `xs`.

For example:

      fruit = "apples" :: ("oranges" :: ("pears" :: Nil))
      nums  = 1 :: (2 :: (3 :: (4 :: Nil)))
      empty = Nil


Right Associativity
===================

Convention: Operators ending in "`:`" associate to the right.

\gap `A :: B :: C` is interpreted as `A :: (B :: C)`.

We can thus omit the parentheses in the definition above.

\example

      val nums = 1 :: 2 :: 3 :: 4 :: Nil

Operators ending in "`:`" are also different in the they are seen as method calls of the _right-hand_ operand.

So the expression above is equivalent to

      Nil.::(4).::(3).::(2).::(1)

Operations on Lists
===================

All operations on lists can be expressed in terms of the following
three operations:

\begin{tabular}{ll}
\verb`head`  &  the first element of the list \\
\verb`tail`  &  the list composed of all the elements except the first. \\
\verb`isEmpty` & `true` if the list is empty, `false` otherwise.
\end{tabular}

These operations are defined as methods of objects of type list.
For example:

      fruit.head      == "apples"
      fruit.tail.head == "oranges"
      diag3.head      == List(1, 0, 0)
      empty.head      == throw NoSuchElementException("head of empty list")

List Patterns
=============

It is also possible to decompose lists with pattern matching.

\begin{tabular}{lp{9cm}}
      \verb`Nil`     &  The \verb`Nil` constant \\
      \verb`p :: ps` &  A pattern that matches a list with a \verb`head` matching \verb`p` and a
                        \verb`tail` matching \verb`ps`. \\
      \verb`List(p1, ..., pn)` & same as \verb`p1 :: ... :: pn :: Nil`
\end{tabular}

\example

\begin{tabular}{lp{9cm}}
      \verb`1 :: 2 :: xs`       & Lists of that start with \verb`1` and then \verb`2`
\\    \verb`x :: Nil`           & Lists of length 1
\\    \verb`List(x)`            & Same as \verb`x :: Nil`
\\    \verb`List()`             & The empty list, same as \verb`Nil`
\\    \verb`List(2 :: xs)`      & A list that contains as only element another list
                                  that starts with \verb`2`.
\end{tabular}

Exercise
========

Consider the pattern `x :: y :: List(xs, ys) :: zs`.

What is the condition that describes most accurately the length `L`
of the lists it matches?

      O         L == 3
      O         L == 4
      O         L == 5
      O         L >= 3
      O         L >= 4
      O         L >= 5

\quiz

Exercise
========

Consider the pattern `x :: y :: List(xs, ys) :: zs`.

What is the condition that describes most accurately the length `L`
of the lists it matches?

      O         L == 3
      O         L == 4
      O         L == 5
      X         L >= 3
      O         L >= 4
      O         L >= 5

Sorting Lists
=============

Suppose we want to sort a list of numbers in ascending order:

 -  One way to sort the list `List(7, 3, 9, 2)` is to sort the
  tail `List(3, 9, 2)` to obtain `List(2, 3, 9)`.
 -  The next step is to insert the head `7` in the right place
  to obtain the result `List(2, 3, 7, 9)`.

This idea describes \red{Insertion Sort} :

      def isort(xs: List[Int]): List[Int] = xs match
        case List() => List()
        case y :: ys => insert(y, isort(ys))

Exercise
========

Complete the definition insertion sort by filling in the `???`s in the definition below:

      def insert(x: Int, xs: List[Int]): List[Int] = xs match
        case List() => ???
        case y :: ys => ???

What is the worst-case complexity of insertion sort relative to the length of the input list `N`?

      O      the sort takes constant time
      O      proportional to N
      O      proportional to N log(N)
      O      proportional to N * N

\quiz

Exercise
========

Complete the definition insertion sort by filling in the `???`s in the definition below:

      def insert(x: Int, xs: List[Int]): List[Int] = xs match
        case List() => List(x)
        case y :: ys =>
          if x < y then x :: xs else y :: insert(x, ys)

What is the worst-case complexity of insertion sort relative to the length of the input list `N`?

      O      the sort takes constant time
      X      proportional to N
      O      proportional to N * log(N)
      O      proportional to N * N

