% Reasoning About Lists
%
%

Laws of Concat
==================

Recall the concatenation operation `++` on lists.

We would like to verify that concatenation is associative, and that it admits the empty list \verb@Nil@ as neutral element to the left and to the right:

       (xs ++ ys) ++ zs  =  xs ++ (ys ++ zs)
              xs ++ Nil  =  xs
              Nil ++ xs  =  xs

\red{Q}: How can we prove properties like these?
->
\red{A}: By \red{structural induction} on lists.


Reminder: Natural Induction
===========================

Recall the principle of proof by \red{natural induction}:

To show a property $\btt P(n)$ for all the integers $\btt n \geq b$,

 - Show that we have $\btt P(b)$ (\red{base case}),
 - for all integers $\btt n \geq b$ show the \red{induction step}:
\begin{quote}
     if one has $\btt P(n)$, then one also has $\btt P(n+1)$.
\end{quote}

Example
=======

Given:

     def factorial(n: Int): Int =
       if n == 0 then 1         // 1st clause
       else n * factorial(n-1)  // 2nd clause

Show that, for all `n >= 4`

      factorial(n) >= power(2, n)

Base Case
=========

\BaseCase{{\tt 4}}

This case is established by simple calculations:

      factorial(4) = 24 >= 16 = power(2, 4)

Induction Step
==============

\InductionStep{\tt n+1}

We have for `n >= 4`:

      factorial(n + 1)
->
      >=  (n + 1) * factorial(n)   // by 2nd clause in factorial
->
      >   2 * factorial(n)         // by calculating
->
      >=  2 * power(2, n)          // by induction hypothesis
->
      =   power(2, n + 1)          // by definition of power

Referential Transparency
========================

Note that a proof can freely apply reduction steps as equalities to some
part of a term.

That works because pure functional programs don't have side effects;
so that a term is equivalent to the term to which it reduces.

This principle is called \red{referential transparency}.


Structural Induction
====================

The principle of structural induction is analogous to natural induction:

To prove a property $\btt P(xs)$ for all lists $\btt xs$,

 -  show that $\btt P(Nil)$ holds (\red{base case}),
 -  for a list $\btt xs$ and some element $\btt x$, show the \red{induction step}:
\begin{quote}
  if $\btt P(xs)$ holds, then $\btt P(x :: xs)$ also holds.
\end{quote}

Example
=======

Let's show that, for lists `xs`, `ys`, `zs`:

       (xs ++ ys) ++ zs  =  xs ++ (ys ++ zs)

To do this, use structural induction on `xs`. From the previous implementation of `concat`,

      def concat[T](xs: List[T], ys: List[T]) = xs match
        case List() => ys
        case x :: xs1 => x :: concat(xs1, ys)

distill two _defining clauses_ of `++`:

             Nil ++ ys  =  ys                   // 1st clause
      (x :: xs1) ++ ys  =  x :: (xs1 ++ ys)     // 2nd clause


Base Case
=========

\BaseCase{\tt Nil}

For the left-hand side we have:

      (Nil ++ ys) ++ zs
->
      =   ys ++ zs        // by 1st clause of ++
->
For the right-hand side, we have:

      Nil ++ (ys ++ zs)
->
      =   ys ++ zs        // by 1st clause of ++

This case is therefore established.

Induction Step: LHS
===================

\InductionStep{\tt x :: xs}

For the left-hand side, we have:

      ((x :: xs) ++ ys) ++ zs
->
      =   (x :: (xs ++ ys)) ++ zs    // by 2nd clause of ++
->
      =   x :: ((xs ++ ys) ++ zs)    // by 2nd clause of ++
->
      =   x :: (xs ++ (ys ++ zs))    // by induction hypothesis

Induction Step: RHS
===================

For the right hand side we have:

      (x :: xs) ++ (ys ++ zs)
->
      =   x :: (xs ++ (ys ++ zs))    // by 2nd clause of ++

So this case (and with it, the property) is established.

Exercise
========

Show by induction on `xs` that `xs ++ Nil  =  xs`.

How many equations do you need for the inductive step?

     O          2
     O          3
     O          4