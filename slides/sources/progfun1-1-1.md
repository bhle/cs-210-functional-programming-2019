% Functional Programming Principles in Scala
% Martin Odersky
% \today

Programming Paradigms
=====================

Paradigm: In science, a \red{paradigm} describes distinct concepts or
thought patterns in some scientific discipline.

Main programming paradigms:

 - imperative programming
 - functional programming
 - logic programming

Orthogonal to it:

 - object-oriented programming

Review: Imperative programming
==============================

Imperative programming is about

 - modifying mutable variables,
 - using assignments
 - and control structures such as if-then-else, loops, break, continue, return.

The most common informal way to understand imperative programs is as instruction sequences for a Von Neumann computer.

Imperative Programs and Computers
=================================

There's a strong correspondence between

\begin{tabular}{lll}
Mutable variables & $\textcolor{navy}{\approx}$ & memory cells \\
Variable dereferences & $\textcolor{navy}{\approx}$ & load instructions \\
Variable assignments & $\textcolor{navy}{\approx}$ & store instructions \\
Control structures & $\textcolor{navy}{\approx}$ & jumps
\end{tabular}
\bigskip

\red{Problem}: Scaling up. How can we avoid conceptualizing programs word by word?

\red{Reference}: John Backus, Can Programming Be Liberated from the von. Neumann Style?, Turing Award Lecture 1978.

Scaling Up
==========

In the end, pure imperative programming is limited by the "Von Neumann"
bottleneck:

\begin{quote}
One tends to conceptualize data structures word-by-word.
\end{quote}

We need other techniques for defining high-level abstractions such as collections, polynomials, geometric shapes, strings, documents.

Ideally: Develop _theories_ of collections, shapes, strings, ...

What is a Theory?
=================

A theory consists of

 - one or more _data types_
 - _operations_ on these types
 - _laws_ that describe the relationships between values and operations

Normally, a theory does not describe mutations!

Theories without Mutation
=========================

For instance the theory of polynomials defines the sum of two polynomials by laws such as:

      (a*x + b) + (c*x + d) = (a + c)*x + (b + d)

But it does not define an operator to change a coefficient while keeping the polynomial the same!
->
Whereas in an imperative program one _can_ write:

\begin{lstlisting}
      class Polynomial { double[] coefficient; }
      Polynomial p = ...;
      p.coefficient[0] = 42;
\end{lstlisting}

Theories without Mutation
=========================

\red{Other example:}

The theory of strings defines a concatenation operator ++ which is associative:

      (a ++ b) ++ c  =  a ++ (b ++ c)

But it does not define an operator to change a sequence element while keeping the sequence the same!

(This one, some languages _do_ get right; e.g. Java's strings are immutable)

Consequences for Programming
============================

If we want to implement high-level concepts following their mathematical theories,
there's no place for mutation.

 - The theories do not admit it.
 - Mutation can destroy useful laws in the theories.

Therefore, let's

 - concentrate on defining theories for operators expressed as functions,
 - avoid mutations,
 - have powerful ways to abstract and compose functions.

Functional Programming
======================

 - In a _restricted_ sense, functional programming (FP) means programming without mutable variables, assignments, loops, and other imperative control structures.
 - In a _wider_ sense, functional programming means focusing on the functions and immutable data.
 - In particular, functions can be values that are produced, consumed, and composed.
 - All this becomes easier in a functional language.

Functional Programming Languages
================================

 - In a _restricted_ sense, a functional programming language is one which does not have mutable variables, assignments, or imperative control structures.
 - In a _wider_ sense, a functional programming language enables the
  construction of elegant programs that focus on functions and immutable data structures.
 - In particular, functions in a FP language are first-class citizens. This means
   \begin{itemize}
   \item they can be defined anywhere, including inside other functions
   \item like any other value, they can be passed as parameters to functions and returned as results
   \item as for other values, there exists a set of operators to compose functions
   \end{itemize}

Some functional programming languages
=====================================

In the restricted sense:

 - Pure Lisp, XSLT, XPath, XQuery, FP
 - Haskell (without I/O Monad or UnsafePerformIO)

In the wider sense:

 - (Lisp, Scheme), Racket, Clojure
 - SML, Ocaml, F\#
 - Haskell (full language)
 - Scala
 - (Smalltalk, Ruby)

(...): languages with first class functions but incomplete support for immutable data

History of FP languages
=======================

\begin{tabular}{llp{2cm}ll}
    1959 & (Lisp)             && 2003 & Scala
\\  1975-77 & ML, FP, Scheme  && 2005 & F\#
\\  1978 & (Smalltalk)        && 2007 & Clojure
\\  1986 & Standard ML        && 2012 & Elixir
\\  1990 & Haskell, Erlang    && 2014 & Swift
\\  2000 & OCaml              && 2017 & Idris
\\       &                    && 2020 & Scala 3
\end{tabular}

Scala 3 is the language we will use in this course.

Recommended Book (1)
====================

Structure and Interpretation of Computer Programs. Harold
Abelson and Gerald J. Sussman. 2nd edition. MIT Press 1996.

\includegraphics[scale=0.4]{images/sicp.jpg}

A classic. Many parts of the course and quizzes are based on it,
but we change the language from Scheme to Scala.

The full text [\blue{can be downloaded here.}](http://mitpress.mit.edu/sicp/)


Recommended Book (2)
====================

Programming in Scala. Martin Odersky, Lex Spoon, and Bill Venners. 3rd edition. Artima 2016.

\includegraphics[scale=0.2]{images/cover.pdf}

The standard language introduction and reference.

Other Recommended Books
=======================

\ \hfill
\includegraphics[scale=0.25]{images/scala-impatient.png} \hfill
\includegraphics[scale=0.33]{images/programming-scala.jpeg} \hfill
\includegraphics[scale=0.45]{images/scala-in-depth.jpeg} \hfill\


