% Functional Programming Principles in Scala
% Martin Odersky
% \today

Keywords
========

This is just for testing syntax highlighting

~~~
abstract  case      catch     class     def       do        else      enum
export    extends   false     final     finally   for       given     if
implicit  import    lazy      match     new       null      object    package
private   protected override  return    super     sealed    then      throw
trait     true      try       type      val       var       while     with
yield
:         =         <-        =>        <:        :>        #         @
=>>       ?=>
~~~

Soft keywords
=============

~~~
as  derives  end  extension  inline  on  opaque  open  transparent  using
*  +  -
~~~