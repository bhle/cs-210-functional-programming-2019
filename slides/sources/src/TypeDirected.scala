
def sort[A](as: List[A])(given o: Ordering[A]): List[A] = {
  def insert(list: List[A], elem: A): List[A] =
    list match {
      case Nil          => elem :: Nil
      case head :: tail =>
        if o.lt(elem, head) then elem :: head :: tail
        else head :: insert(tail, elem)
    }
  as.foldLeft(Nil)(insert)
}

case class Rational(num: Int, denom: Int)

// Implement an instance of the Ordering type class for Rational

object Rational {
  given Ordering[Rational] {
    // Note: it doesn’t work with negative quotients and doesn’t handle a Int overflows
    // Addressing these issues is left as an exercise to the reader
    def compare(p: Rational, q: Rational): Int =
      p.num * q.denom - q.num * p.denom
  }

  def fromInt(x: Int) = Rational(x, 1)
}

// Implement an instance of the Ordering type class for pairs of type (A, B)

given [A, B](given ordA: Ordering[A], ordB: Ordering[B]): Ordering[(A, B)] {
  def compare(p1: (A, B), p2: (A, B)): Int = {
    val c = ordA.compare(p1._1, p2._1)
    if c == 0 then ordB.compare(p1._2, p2._2)
    else c
  }
}

// Define a Ring type class

trait Ring[A] {
  def plus(a1: A, a2: A): A
  def mul(a1: A, a2: A): A
  def zero: A
  // ... to be continued
}

// Implement a function that checks that the + associativy law is satisfied
// by a given Ring instance

def associativeRing[A](a1: A, a2: A, a3: A)(given ring: Ring[A]): Boolean =
  ring.plus(a1, ring.plus(a2, a3)) == ring.plus(ring.plus(a1, a2), a3)

// Given conversion examples

sealed trait Json
case class JNumber(value: BigDecimal) extends Json
case class JString(value: String) extends Json
case class JBoolean(value: Boolean) extends Json
case class JArray(elems: List[Json]) extends Json
case class JObject(fields: (String, Json)*) extends Json

object Json {

  def obj(fields: (String, JsonField)*): Json =
    JObject(fields.map { (k, v) => (k, v.json) }: _*)

  class JsonField(val json: Json)

  object JsonField {
    given intIsJsonField: Conversion[Int, JsonField] {
      def apply(x: Int): JsonField = new JsonField(JNumber(x))
    }
  }
}


@main def main = {
  println(Json.obj("foo" -> 42))
}
