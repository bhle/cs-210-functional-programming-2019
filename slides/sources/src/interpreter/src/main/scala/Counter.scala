object Counter
  private var count: BigInt = 0

  def reset() =
    count = 0

  def inc() =
    count = count + 1

  def get = count
  