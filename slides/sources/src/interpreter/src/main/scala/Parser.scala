import scallion.input._
import scallion.lexical._
import scallion.syntactic._

object Tokens 
  enum Keyword
    case Def, If, Then, Else

  enum Delimiter
    case Parens(isOpen: Boolean)
    case Equal, RightArrow

  enum Token
    case KeywordToken(keyword: Keyword)
    case DelimiterToken(delimiter: Delimiter)
    case LiteralToken(value: BigInt)
    case IdentifierToken(name: String)
    case IgnoredToken
    case ErrorToken(error: String)

  enum Kind
    case KeywordKind(keyword: Keyword)
    case DelimiterKind(delimiter: Delimiter)
    case OperatorKind, LiteralKind, IdentifierKind, IgnoredKind, ErrorKind

  object Kind
    import Token._
    import Kind._

    def of(token: Token): Kind = token match
      case KeywordToken(keyword) => KeywordKind(keyword)
      case DelimiterToken(delimiter) => DelimiterKind(delimiter)
      case LiteralToken(_) => LiteralKind
      case IdentifierToken(_) => IdentifierKind
      case IgnoredToken => IgnoredKind
      case ErrorToken(_) => ErrorKind
end Tokens

import Tokens._

object Lexer extends Lexers[Token, Char, Unit] with CharRegExps

  import Token._
  import Kind._
  import Keyword._
  import Delimiter._

  val lexer = Lexer(

    // Keywords
    word("def")  |> { (cs, r) => KeywordToken(Def) },
    word("if")   |> { (cs, r) => KeywordToken(If) },
    word("then") |> { (cs, r) => KeywordToken(Then) },
    word("else") |> { (cs, r) => KeywordToken(Else) },

    // Delimiters
    elem('=')  |> { (cs, r) => DelimiterToken(Equal) },
    word("=>") |> { (cs, r) => DelimiterToken(RightArrow) },
    elem('(')  |> { (cs, r) => DelimiterToken(Parens(true)) },
    elem(')')  |> { (cs, r) => DelimiterToken(Parens(false)) },

    // Operators
    many1(oneOf("+-*/%^#=<>:"))  |> { (cs, r) => IdentifierToken(cs.mkString) },

    // Identifiers
    (elem(_.isLetter) | elem('_')) ~ many(elem(_.isLetterOrDigit) | elem('_'))
      |> { (cs, r) => IdentifierToken(cs.mkString) },

    // Numbers
    nonZero ~ many(digit) | elem('0')
      |> { (cs, r) => LiteralToken(BigInt(cs.mkString)) },

    // Spaces
    many(oneOf(" \t\r\n")) |> { (cs, r) => IgnoredToken },
  ) onError {
    (cs, r) => ErrorToken(cs.mkString)
  }

  def apply(chars: Iterator[Char]): Iterator[Token] =
    val input = Source.fromIterator(chars, NoPositioner)
    lexer(input).filter(Kind.of(_) != IgnoredKind)

end Lexer

object Parser extends Syntaxes[Token, Kind]

  import scala.language.implicitConversions

  import Token._
  import Kind._
  import Keyword._
  import Delimiter._

  import I07Defs._
  import Expr._

  override def getKind(token: Token): Kind = Kind.of(token)

  implicit def kw(keyword: Keyword): Syntax[Unit] = elem(KeywordKind(keyword)).unit(KeywordToken(keyword))

  implicit def del(delimiter: Delimiter): Syntax[Unit] = elem(DelimiterKind(delimiter)).unit(DelimiterToken(delimiter))

  val lit: Syntax[Expr] = accept(LiteralKind) {
    case LiteralToken(value) => C(value)
  }

  val id: Syntax[String] = accept(IdentifierKind) {
    case IdentifierToken(value) => value
  }

  val name: Syntax[Expr] = id.map(N(_))

  val basicExpr: Syntax[Expr] = name | lit

  lazy val complexExpr: Syntax[Expr] = recursive {
    val fromId: Syntax[Expr] = {
      (id ~ ((RightArrow.skip ~ complexExpr) || many(expr))).map {
        case name ~ Left(expr) => Fun(name, expr)
        case name ~ Right(exprs) => {
          (N(name) +: exprs).reduceLeft(Call(_, _))
        }
      }
    }

    val fromOthers: Syntax[Expr] =
      ((lit | parensExpr) ~ many(expr)).map {
        case e ~ es => (e +: es).reduceLeft(Call(_, _))
      }

    val ifExpr: Syntax[Expr] =
      (If.skip ~ complexExpr ~ Then.skip ~ complexExpr ~ Else.skip ~ complexExpr).map {
        case c ~ t ~ e => IfNonzero(c, t, e)
      }

    fromId | fromOthers | ifExpr
  }

  lazy val definition: Syntax[(String, Expr)] = 
    (Def.skip ~ id ~ Equal.skip ~ expr).map {
      case n ~ e => (n, e)
    }

  lazy val parensExpr: Syntax[Expr] = recursive {
    (Parens(true) ~>~ many(definition) ~ complexExpr ~<~ Parens(false)).map {
      case ds ~ e if ds.isEmpty => e
      case ds ~ e => Defs(ds.toList, e)
    }
  }

  lazy val expr: Syntax[Expr] = recursive {
    basicExpr | parensExpr
  }


end Parser