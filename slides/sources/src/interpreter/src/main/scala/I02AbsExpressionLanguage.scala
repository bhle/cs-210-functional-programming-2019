object I02AbsExpressionLanguage

  enum Expr 
    case C(c: BigInt)
    case BinOp(op: BinOps, e1: Expr, e2: Expr)
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr)
    case AbsValue(arg: Expr)
  end Expr
  import Expr._

  enum BinOps
    case Plus, Minus, Times, Power, LessEq
  import BinOps._

  def evalBinOp(op: BinOps)(x: BigInt, y: BigInt): BigInt = op match
    case Plus => x + y
    case Minus => x - y
    case Times => x * y
    case Power => x.pow(y.toInt)
    case LessEq => if (x <= y) 1 else 0

  def eval(e: Expr): BigInt = e match 
    case C(c) => c
    case BinOp(op, e1, e2) =>
      evalBinOp(op)(eval(e1), eval(e2))
    case IfNonzero(cond, trueE, falseE) =>
      if eval(cond) != 0 then eval(trueE)
      else eval(falseE)
    case AbsValue(arg) => throw Exception("Cannot eval Abs")
  
  val expr4 = AbsValue(BinOp(Plus,C(10),C(-50)))
  val expr5 = AbsValue(BinOp(Minus,AbsValue(C(-10)),C(12)))

  def desugar(e: Expr): Expr = e match
    case C(c) => e
    case BinOp(op, e1, e2) => BinOp(op, desugar(e1), desugar(e2))
    case IfNonzero(cond, trueE, falseE) =>
      IfNonzero(desugar(cond), desugar(trueE), desugar(falseE))
    case AbsValue(arg) =>
      val x = desugar(arg)
      IfNonzero(BinOp(LessEq, x, C(0)), 
                BinOp(Minus, C(0), x),
                x)  

  def strOp(op: BinOps): String = op match
    case Plus => "+"
    case Minus => "-"
    case Times => "*"
    case Power => "^"
    case LessEq => "<="
  
  def str(e: Expr): String = e match 
    case C(c) => c.toString
    case BinOp(op, e1, e2) =>
      s"(${strOp(op)} ${str(e1)} ${str(e2)})"
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    case AbsValue(arg) => s"(abs ${str(arg)})"

  def show(e: Expr): Unit =
    println("original:")
    println(str(e))
    val de = desugar(e)
    println("desugared:")
    println(str(de))
    println(" ~~> " + eval(de) + "\n")
  
  def main(args: Array[String]): Unit = 
    show(expr4)
    show(expr5)

end I02AbsExpressionLanguage
