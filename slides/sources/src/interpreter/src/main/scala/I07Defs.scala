object I07Defs
  enum Expr 
    case C(c: BigInt)
    case N(name: String) 
    case IfNonzero(cond: Expr, trueE: Expr, falseE: Expr) 
    case Call(function: Expr, arg: Expr)
    case Fun(formal: String, body: Expr)
    case Defs(defs: List[(String, Expr)], rest: Expr)
  end Expr
  import Expr._

  val lDefs = List(
    "nDiv" -> Fun("xCurrent", Fun("acc",
       IfNonzero(leq(N("y"), N("xCurrent")),
         Call(Call(N("nDiv"), minus(N("xCurrent"), N("y"))),
              plus(C(1), N("acc"))),
         N("acc"))))
  )  
  val tDivExpr = 
        Defs(List("tDiv" -> Fun("x", Fun("y",
                            Defs(lDefs, Call(Call(N("nDiv"), N("x")), C(0)))))),
             Call(Call(N("tDiv"), C(720)), C(10)))

  enum Value
    case I(i: BigInt)
    case F(f: Value => Value)
  end Value

  def bin(op: String)(e1: Expr, e2: Expr) = Call(Call(N(op), e1), e2)    
  def minus = bin("-")
  def plus  = bin("+")
  def leq   = bin("<=")
  def times = bin("*")

  val gDefs = List[(String, Expr)]( // used to construct bigger expressions
    "fact" -> Fun("n",
       IfNonzero(N("n"),
         times(N("n"), 
               Call(N("fact"), minus(N("n"), C(1)))),
         C(1))),
    "div" -> Fun("x", Fun("y",
       IfNonzero(leq(N("y"), N("x")),
         plus(C(1), 
              Call(Call(N("div"), minus(N("x"), N("y"))),
                        N("y"))),
         C(0)))),
    "twice" -> Fun("f", Fun("x",
      Call(N("f"), Call(N("f"), N("x"))))),
    "twice1" -> Fun("f", Fun("fact",
      Call(N("f"), Call(N("f"), N("fact"))))),
    "square" -> Fun("x", times(N("x"), N("x")))
  )
  val factExpr1 = Call(N("fact"), C(6))
  val divExpr2 = Call(Call(N("div"), C(15)), C(6))
  val expr3 = Call(Call(N("div"), factExpr1), C(10))
  val expr4 = Call(Call(N("twice"), N("square")), C(3))
  val expr5 = Call(Call(N("twice1"), N("fact")), C(3))

  type Env = String => Option[Value]

  def lift2int(f: (BigInt, BigInt) => BigInt): Option[Value] =
    import Value._
    Some(F(
      (v1:Value) => F(
        (v2:Value) => {
          (v1,v2) match 
            case (I(i1),I(i2)) => I(f(i1,i2))
            case _ => error("Wrong operator type")
        })))

  val initEnv: Env = 
    (s:String) => s match
      case "+"  => lift2int(_ + _)
      case "-"  => lift2int(_ - _)
      case "*"  => lift2int(_ * _)
      case "^"  => lift2int((x:BigInt,y:BigInt) => x.pow(y.toInt))
      case "<=" => lift2int(
        (x:BigInt,y:BigInt) => if x <= y then 1 else 0)
      case _ => error(s"Unknown name '$s' in initial environment")
  
  def evalEnv(e: Expr, env: Env): Value = e match
    case C(c) => Value.I(c)
    case N(n) => env(n) match
      case Some(v) => v
      case None => error(s"Unknown name $n")
    case IfNonzero(cond, trueE, falseE) =>
      Counter.inc()
      if evalEnv(cond,env) != Value.I(0) then evalEnv(trueE,env)
      else evalEnv(falseE,env)
    case Fun(n,body) => Value.F{(v: Value) =>
      val env1: String => Option[Value] = 
        (s:String) => if s==n then Some(v) else env(s)
      evalEnv(body, env1) }
    case Call(fun, arg) => evalEnv(fun,env) match 
      case Value.F(f) => Counter.inc(); f(evalEnv(arg,env))
      case _ => error(s"Non-function arg $fun in $fun($arg)")
    case Defs(defs, rest) =>
      def env1: Env = 
        (s:String) =>
          lookup(defs, s) match 
            case None => env(s)
            case Some(body) => Some(evalEnv(body, env1))
      evalEnv(rest, env1)  

  def lookup[K,V](kvs: List[(K,V)], k: K): Option[V] =
    kvs match 
      case Nil => None
      case (k1,v1)::kvs1 => 
        if k==k1 then Some(v1) else lookup(kvs1,k)

  def error(s: String): Nothing = throw Exception(s)

  // Printing and displaying
    
  def str(e: Expr): String = 
   e match 
    case C(c) => c.toString
    case N(n) => n    
    case IfNonzero(cond, trueE, falseE) =>
      s"(if ${str(cond)} then ${str(trueE)} else ${str(falseE)})"
    case Call(_, _) => "(" + calls(e) + ")"
    case Fun(_, _) => "(" + funs(e) + ")"
    case Defs(defs, rest) => 
      val defStrs = 
        for ((name,body) <- defs)
        yield s"  def $name =\n    ${str(body)}"
      s"(\n${defStrs.mkString("\n")}\n  ${str(rest)}\n)"
  end str

  def calls(e: Expr): String = e match 
    case Call(f, arg) => calls(f) + " " + str(arg)
    case _ => str(e)

  def funs(e: Expr): String = e match
    case Fun(n,body) => n + " => " + funs(body)
    case _ => str(e)

  def strVal(v: Value): String =
    v match 
      case Value.I(i) => i.toString
      case Value.F(f) => "<function>"

  def show(e: Expr): Unit =
    Counter.reset()
    println(str(e))
    val res: Value = evalEnv(e,initEnv)
    println(s"  (${Counter.get} steps) ~~> ${strVal(res)}\n")

  def showD(e: Expr): Unit =
    show(Defs(gDefs, e))

  def main(args: Array[String]): Unit =    
    show(plus(C(3),C(4)))
    showD(Call(N("square"), C(5)))
    showD(Call(N("square"), Call(N("square"),C(3))))
    showD(Call(N("fact"), C(0)))
    showD(factExpr1) 
    showD(divExpr2)
    showD(expr3)    
    showD(expr4) 
    showD(expr5) 
    show(tDivExpr)

end I07Defs