object I08Repl

  def evalStr(input: String): Unit =
    val tokens = Lexer(input.iterator)
    val result = Parser.complexExpr(tokens)
    result match {
      case Parser.Parsed(tree,rest) => 
        import I07Defs._
        Counter.reset()
        println(str(tree))
        val res: Value = evalEnv(tree,initEnv)
        println(s"  (${Counter.get} steps) ~~> ${strVal(res)}\n")
      case _ => println(result)
    } 

  def repl(): Unit =
    println("Enter expression. Empty line to quit.")
    print("program>")
    val input: String = scala.io.StdIn.readLine()
    if input != "" then
      evalStr(input)
      repl()

  val inputNDiv: String = """
      (def tDiv = (x => y =>
                    (def nDiv = (xCurrent => acc =>
                                  (if <= y xCurrent then nDiv (- xCurrent y)
                                                              (+ 1 acc)
                                                     else acc))
                    nDiv x 0))
       tDiv 720 10)"""

  val factRecursive: String = """
       (def fact = (n =>
                     if n then * n (fact (- n 1)) else 1)
         fact 10)
   """
  // By the way, we don't really need recursion

  val factSelf: String = """
      (def factGen = (self => n =>
                    if n then * n (self self (- n 1)) else 1)       
       factGen factGen 10)
  """

  // This also works in Scala, we just need to define recursive type

  case class T(f: T => BigInt => BigInt)

  val factGen: T = T(
    (self:T) =>
      (n:BigInt) =>
        if n != 0 then n * self.f(self)(n - 1)
                  else 1
  )
  def factOf10: BigInt = factGen.f(factGen)(10)
  def fact(m: BigInt) = factGen.f(factGen)(m)

  def main(args: Array[String]): Unit =
    println("factOf10 = " + factOf10)
    println("fact(10) = " + fact(10))
    //evalStr(inputNDiv)
    evalStr(factRecursive)
    evalStr(factSelf)
    if !Parser.complexExpr.isLL1 then println("Warning: Not LL1")        
    repl()

end I08Repl