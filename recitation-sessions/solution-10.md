## Recitation session 10, solutions

### Exercise 1

```scala
def succ = (n => f => x => f(n(f))(x))
```

### Exercise 2

```scala
def add = (n => m => n(succ)(m))
```

### Exercise 3

```scala
def size = (l => l(zero)( h => t => succ(size(t)) ))
```

### Exercise 4

```scala
def map = (f => l =>
           l(nil)( h => t => cons( f(h) )( map(f)(t) ) ))
```